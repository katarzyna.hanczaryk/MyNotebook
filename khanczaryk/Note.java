package khanczaryk;

import java.time.LocalDate;
import java.util.Set;

/**
 * Contains all information for a note.
 *
 * @author kHanczaryk
 * @version %I%, %G%
 */
public class Note {

    private String title;
    private String note;
    private LocalDate date;
    private String podcast;
    private Set<String> tags;

    Note(String title, String note, LocalDate date, String podcast, Set<String> tags) {
        this.title = title;
        this.note = note;
        this.date = date;
        this.podcast = podcast;
        this.tags = tags;
    }


    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    String getNote() {
        return note;
    }

    void setNote(String note) {
        this.note = note;
    }

    LocalDate getDate() {
        return date;
    }

    void setDate(LocalDate date) {
        this.date = date;
    }

    String getPodcast() {
        return podcast;
    }

    void setPodcast(String podcast) {
        this.podcast = podcast;
    }

    Set<String> getTags() {
        return tags;
    }

    void setTags(Set<String> tags) {
        this.tags = tags;
    }

    /**
     * Checks if a note has only empty fields.
     *
     * @return <code>true</code> when all fields are empty. Otherwise <code>false</code>
     */
    boolean isIncomplete() {
        String[] tagsArray = this.getTags().toArray(new String[this.getTags().size()]);
        return (title.equals("") || note.equals("") || podcast.equals("") || date == null || tagsArray[0].equals(""));
    }

    @Override
    public String toString() {
        String tagsList = "";
        for (String tag : tags) {
            tagsList += tag + " ";
        }
        return "title = " + title + '\n' +
                "note = " + note + '\n' +
                "date = " + date + '\n' +
                "podcast = " + podcast + '\n' +
                "tags = " + tagsList;
    }
}
