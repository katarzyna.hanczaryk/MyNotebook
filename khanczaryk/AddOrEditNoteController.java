package khanczaryk;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * Shows the fields to fill in with data for the new note; accepts the user input: title, note, date, podcast and
 * tags.
 *
 * @author kHanczaryk
 * @version %I%, %G%
 */
public class AddOrEditNoteController {

    @FXML
    public BorderPane borderPaneAdd;

    @FXML
    private TextField newTitle;

    @FXML
    private TextArea newNote;

    @FXML
    private DatePicker newDate;

    @FXML
    private TextField newPodcast;

    @FXML
    private TextField newTags;

    @FXML
    private VBox vBoxAdd;

    @FXML
    private MainController myMainController;

    @FXML
    VBox getvBoxAdd() {
        return vBoxAdd;
    }

    @FXML
    BorderPane getBorderPaneAdd() {
        return borderPaneAdd;
    }

    @FXML
    TextField getNewTitle() {
        return newTitle;
    }

    @FXML
    TextArea getNewNote() {
        return newNote;
    }

    @FXML
    DatePicker getNewDate() {
        return newDate;
    }

    @FXML
    TextField getNewPodcast() {
        return newPodcast;
    }

    @FXML
    TextField getNewTags() {
        return newTags;
    }

    @FXML
    void setMyMainController(MainController myMainController) {
        this.myMainController = myMainController;
    }

    /**
     * Creates a new <code>Note</code> from the data given by the user in <code>TextAreas</code> in
     * <code>DialogPane</code> for adding a new <code>Note</code> upon the click the <code>Button</code>"Add note"
     *
     * @return Note new Note created from the input given by the user
     * @see MainController#addNoteClicked()
     */
    @FXML
    Note getNoteContent() {
        return myMainController.createNoteFromUserInput(newTitle, newNote, newDate, newPodcast, newTags);
    }
}