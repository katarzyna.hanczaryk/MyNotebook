package khanczaryk;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Creates one database which contains 3 tables: podcasts (with a name column), notes (with columns: title, note, date,
 * podcast) and tags (with a note column). Adds and removes notes. Shows all tags, all podcasts, all notes
 * from MyNotebook.db. Shows all information for a given title. Shows all titles for a given podcast or a given tag.
 * Tells if a given title, tag or podcast exists already in the database.
 *
 * @author kHanczaryk
 * @version %I%, %G%
 */
public class Database {

    private final String CONNECTION_ROOT = "jdbc:sqlite:" + System.getProperty("user.home") + File.separator +
            "myNotebook.db";

    private static final String TABLE_PODCASTS = "podcasts";
    private static final String COLUMN_PODCAST_ID = "ROWID";
    private static final String COLUMN_PODCAST_NAME = "name";

    private static final String TABLE_NOTES = "notes";
    private static final String COLUMN_NOTE_ID = "ROWID";
    private static final String COLUMN_NOTE_TITLE = "title";
    private static final String COLUMN_NOTE_NOTE = "note";
    private static final String COLUMN_NOTE_DATE = "date";
    private static final String COLUMN_NOTE_PODCAST = "podcast";

    private static final String TABLE_TAGS = "tags";
    private static final String COLUMN_TAGS_TAGS = "tag";
    private static final String COLUMN_TAGS_NOTE = "note";

    private static Database instance = new Database();

    private Connection conn;

    /**
     * The constructor of this class. Is private and with <code>getInstance()</code> allows the use of the singleton
     * pattern. Creates TableNotes, TablePodcasts and TableTags in MyNotebook.db.
     */
    private Database() {
        this.openConnection();
        try (Statement statement = conn.createStatement()) {

            String CREATE_TABLE_NOTES = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTES + " (" + COLUMN_NOTE_TITLE +
                    " TEXT NOT NULL, " + COLUMN_NOTE_NOTE + " TEXT , " + COLUMN_NOTE_DATE + " DATE, " +
                    COLUMN_NOTE_PODCAST + " INTEGER, PRIMARY KEY (" + COLUMN_NOTE_TITLE + ") )";
            statement.execute(CREATE_TABLE_NOTES);

            String CREATE_TABLE_PODCASTS = "CREATE TABLE IF NOT EXISTS " + TABLE_PODCASTS + " (" +
                    COLUMN_PODCAST_NAME + " TEXT NOT NULL, PRIMARY KEY (" + COLUMN_PODCAST_NAME + ") )";
            statement.execute(CREATE_TABLE_PODCASTS);

            String CREATE_TABLE_TAGS = "CREATE TABLE IF NOT EXISTS " + TABLE_TAGS + " (" + COLUMN_TAGS_TAGS +
                    " TEXT, " + COLUMN_TAGS_NOTE + " INTEGER)";
            statement.execute(CREATE_TABLE_TAGS);

        } catch (SQLException e) {
            System.out.println("The database was not created. " + e.getMessage());
        }
    }

    static Database getInstance() {
        return instance;
    }

    /**
     * Opens connection with the database.
     *
     * @return <code>true</code> when the connection was set. Otherwise <code>false</code>
     * @see Main#init()
     */
    boolean openConnection() {
        try {
            conn = DriverManager.getConnection(CONNECTION_ROOT);
            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't connect to database: " + e.getMessage());
            return false;
        }
    }

    /**
     * Closes the connection with the database, it it exists.
     *
     * @return <code>true</code> when connection is closed. Otherwise <code>false</code>
     * @see Main#stop()
     */
    boolean closeConnection() {
        try {
            if (conn != null) {
                conn.close();
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Couldn't close connection " + e.getMessage());
        }
        return false;
    }

    /**
     * Adds information about a new note to PodcastTable, TagTable and NoteTable. Adds a new Podcast to a PodcastTable
     * if a podcast did not exist before.
     *
     * @param noteToAdd Note to add to the database
     * @see MainController
     */
        void addNewNote(Note noteToAdd) {
        PreparedStatement addNewPodcastToPodcastTable;
        PreparedStatement addNewNoteToNoteTable;
        PreparedStatement queryForPodcastID;
        PreparedStatement queryForNoteID;
        PreparedStatement addAllFieldsTagTable;

        try {
            {
                String ADD_PODCAST_PODCAST_TABLE = "INSERT OR IGNORE INTO " + TABLE_PODCASTS + " ("
                        + COLUMN_PODCAST_NAME + ") VALUES(?)";
                addNewPodcastToPodcastTable = conn.prepareStatement(ADD_PODCAST_PODCAST_TABLE);
                addNewPodcastToPodcastTable.setString(1, noteToAdd.getPodcast());
                addNewPodcastToPodcastTable.execute();
                addNewPodcastToPodcastTable.close();
            }
            {
                String ADD_ALL_FIELDS_NOTE_TABLE = "INSERT OR IGNORE INTO " + TABLE_NOTES + " (" + COLUMN_NOTE_TITLE
                        + ", " + COLUMN_NOTE_NOTE + ", " + COLUMN_NOTE_DATE + ", " + COLUMN_NOTE_PODCAST +
                        ") VALUES (?, ?, ?, ?)";

                addNewNoteToNoteTable = conn.prepareStatement(ADD_ALL_FIELDS_NOTE_TABLE);
                addNewNoteToNoteTable.setString(1, noteToAdd.getTitle());
                addNewNoteToNoteTable.setString(2, noteToAdd.getNote());
                addNewNoteToNoteTable.setDate(3, java.sql.Date.valueOf(noteToAdd.getDate()));

                String QUERY_FOR_PODCAST_ID = "SELECT " + COLUMN_PODCAST_ID + " FROM " + TABLE_PODCASTS + " WHERE " +
                        COLUMN_PODCAST_NAME + "=?";

                queryForPodcastID = conn.prepareStatement(QUERY_FOR_PODCAST_ID);
                queryForPodcastID.setString(1, noteToAdd.getPodcast());

                // no possibility for more than 1 result due to no duplicates in podcasts table
                ResultSet resultSet = queryForPodcastID.executeQuery();
                String str = resultSet.getString(COLUMN_PODCAST_ID);
                int podcastID = Integer.parseInt(str);
                queryForPodcastID.close();

                addNewNoteToNoteTable.setInt(4, podcastID);
                addNewNoteToNoteTable.execute();
                addNewNoteToNoteTable.close();
            }
            {
                String QUERY_FOR_NOTE_ID = "SELECT " + COLUMN_NOTE_ID + " FROM " + TABLE_NOTES + " WHERE " +
                        COLUMN_NOTE_NOTE + "=?";

                queryForNoteID = conn.prepareStatement(QUERY_FOR_NOTE_ID);
                queryForNoteID.setString(1, noteToAdd.getNote());
                ResultSet rsNoteID = queryForNoteID.executeQuery();
                String str = rsNoteID.getString(COLUMN_NOTE_ID);
                int noteID = Integer.parseInt(str);

                String ADD_ALL_FIELDS_TAG_TABLE = "INSERT INTO " + TABLE_TAGS + " (" + COLUMN_TAGS_TAGS + ", " +
                        COLUMN_TAGS_NOTE + ") VALUES(?,?)";
                addAllFieldsTagTable = conn.prepareStatement(ADD_ALL_FIELDS_TAG_TABLE);
                addAllFieldsTagTable.setInt(2, noteID);
                queryForNoteID.close();

                for (String tag : noteToAdd.getTags()) {
                    addAllFieldsTagTable.setString(1, tag);
                    addAllFieldsTagTable.execute();
                }
                addAllFieldsTagTable.close();
            }
        } catch (SQLException e) {
            System.out.println("The new note cannot be added " +
                    e.getMessage());
        }
    }

    /**
     * Removes note which has a given title from MyNotebook.db. Finds ROWID of a note to remove based on the given
     * title, removes all tags from the TagTable with a ROWID of a note to remove, removes a podcast from PodcastTable
     * if the podcast is associated only with the note to remove, and delete the note from the Notetable.
     *
     * @param noteTitleToRemove The title of the note which is to be removed
     * @see MainController#removeClicked()
     * @see MainController#editClicked()
     */
    void removeNote(String noteTitleToRemove) {
        String ROWIDnoteToRemove;
        String FIND_ROWID_OF_NOTE_TO_REMOVE = "SELECT " + COLUMN_NOTE_ID + " FROM " + TABLE_NOTES + " WHERE " +
                COLUMN_NOTE_TITLE + " = ?";
        String REMOVE_TAGS_FROM_TAGS_TABLE = "DELETE FROM " + TABLE_TAGS + " WHERE " + COLUMN_TAGS_NOTE + " = ?";
        PreparedStatement removeTagsFromTagsTable;
        PreparedStatement findRowidOfNoteToRemove;
        try {
            findRowidOfNoteToRemove = conn.prepareStatement(FIND_ROWID_OF_NOTE_TO_REMOVE);
            findRowidOfNoteToRemove.setString(1, noteTitleToRemove);
            ResultSet noteROWIDfound = findRowidOfNoteToRemove.executeQuery();
            ROWIDnoteToRemove = noteROWIDfound.getString(COLUMN_NOTE_ID);
            removeTagsFromTagsTable = conn.prepareStatement(REMOVE_TAGS_FROM_TAGS_TABLE);
            removeTagsFromTagsTable.setString(1, ROWIDnoteToRemove);
            removeTagsFromTagsTable.execute();
            findRowidOfNoteToRemove.close();
            removeTagsFromTagsTable.close();
        } catch (SQLException e) {
            System.out.println("It's impossible to remove tags");
            e.printStackTrace();
        }

        String foundPodcastID;
        String FIND_PODCAST_OF_NOTE_TO_REMOVE = "SELECT " + COLUMN_NOTE_PODCAST + " FROM " + TABLE_NOTES + " WHERE "
                + COLUMN_NOTE_TITLE + " = ?";
        String COUNT_NOTES_FOR_PODCAST = "SELECT COUNT (*) AS total FROM " + TABLE_NOTES + " WHERE " +
                COLUMN_NOTE_PODCAST + " = ?";
        String REMOVE_PODCAST_FROM_PODCAST_TABLE = "DELETE FROM " + TABLE_PODCASTS + " WHERE "
                + COLUMN_PODCAST_ID + "= ?";
        PreparedStatement findPodcastNoteToRemove;
        PreparedStatement countNotesForPodcast;
        PreparedStatement removePodcastFromPodcastTable;
        try {
            findPodcastNoteToRemove = conn.prepareStatement(FIND_PODCAST_OF_NOTE_TO_REMOVE);
            findPodcastNoteToRemove.setString(1, noteTitleToRemove);
            ResultSet podcastIDFound = findPodcastNoteToRemove.executeQuery();
            foundPodcastID = podcastIDFound.getString(COLUMN_NOTE_PODCAST);
            countNotesForPodcast = conn.prepareStatement(COUNT_NOTES_FOR_PODCAST);
            countNotesForPodcast.setString(1, foundPodcastID);
            ResultSet numberOfNotesFound = countNotesForPodcast.executeQuery();
            int numberOfNotes = numberOfNotesFound.getInt("total");
            if (numberOfNotes == 1) {
                removePodcastFromPodcastTable = conn.prepareStatement(REMOVE_PODCAST_FROM_PODCAST_TABLE);
                removePodcastFromPodcastTable.setString(1, foundPodcastID);
                removePodcastFromPodcastTable.execute();
                removePodcastFromPodcastTable.close();
            }
            findPodcastNoteToRemove.close();
            countNotesForPodcast.close();
        } catch (SQLException e) {
            System.out.println("It was impossible to remove podcast");
            e.printStackTrace();
        }

        String DELETE_NOTE = "DELETE FROM " + TABLE_NOTES + " WHERE " + COLUMN_NOTE_TITLE + " = ?";
        PreparedStatement deleteNote;
        try {
            deleteNote = conn.prepareStatement(DELETE_NOTE);
            deleteNote.setString(1, noteTitleToRemove);
            deleteNote.execute();
            deleteNote.close();
        } catch (SQLException e) {
            System.out.println("It is impossible to remove the note for given title");
            e.printStackTrace();
        }
    }

    /**
     * Shows a list of all tags present in MyNotebook.db
     *
     * @return ObservableList of all tags from MyNotebook.db
     * @see MainController#tagsClicked()
     */
    ObservableList<String> findAllTags() {
        String FIND_ALL_TAGS = "SELECT " + COLUMN_TAGS_TAGS + " FROM " + TABLE_TAGS;
        Set<String> allTags = new HashSet<>();
        try (Statement statement = conn.createStatement();
             ResultSet tagsSet = statement.executeQuery(FIND_ALL_TAGS)) {
            while (tagsSet.next()) {
                String tag = tagsSet.getString(COLUMN_TAGS_TAGS);
                allTags.add(tag);
            }
            ObservableList<String> myTags = FXCollections.observableArrayList();
            myTags.addAll(allTags);
            return myTags;
        } catch (SQLException e) {
            System.out.println("It is impossible to print tags " + e.getMessage());
        }
        return null;
    }

    /**
     * Shows all titles kept in MyNotebook.db.
     *
     * @return ObservableList of all titles in MyNotebook.db
     * @see MainController
     */
    ObservableList<String> findAllNotes() {
        String FIND_ALL_NOTES = "SELECT " + COLUMN_NOTE_TITLE + " FROM " + TABLE_NOTES;
        ObservableList<String> noteList = FXCollections.observableArrayList();

        try (Statement statement = conn.createStatement();
             ResultSet notesList = statement.executeQuery(FIND_ALL_NOTES)) {
            while (notesList.next()) {
                String tempNote = notesList.getString(COLUMN_NOTE_TITLE);
                noteList.add(tempNote);
            }
            return noteList;
        } catch (SQLException e) {
            System.out.println("It is impossible to print notes " + e.getMessage());
        }
        return null;
    }

    /**
     * Finds <code>Note</code> for a given title.
     *
     * @param noteTitle Title for which note is to be found
     * @return Note for a given title
     * @see #findTagsForNote(String)
     * @see MainController#elementClicked()
     * @see MainController#editClicked()
     *
     */
    Note findAllInfoForNote(String noteTitle) {
        String FIND_ALL_NOTES_INFO_FOR_TAG = "SELECT " + COLUMN_NOTE_DATE + ", " + TABLE_PODCASTS + "." +
                COLUMN_PODCAST_NAME + ", " + TABLE_NOTES + "." + COLUMN_NOTE_NOTE + " FROM " +
                TABLE_TAGS + " INNER JOIN " + TABLE_NOTES + " ON " + TABLE_TAGS + "." +
                COLUMN_TAGS_NOTE + " = " + TABLE_NOTES + "." + COLUMN_NOTE_ID + " INNER JOIN " +
                TABLE_PODCASTS + " ON " + TABLE_NOTES + "." + COLUMN_NOTE_PODCAST + " = " + TABLE_PODCASTS + "." +
                COLUMN_PODCAST_ID + " WHERE " + COLUMN_NOTE_TITLE + " = ?";

        PreparedStatement findAllNotesInfoForNote;
        String note = "";
        LocalDate noteDate = null;
        String podcast = "";
        Set<String> tagsSet = this.findTagsForNote(noteTitle);
        try {
            findAllNotesInfoForNote = conn.prepareStatement(FIND_ALL_NOTES_INFO_FOR_TAG);
            findAllNotesInfoForNote.setString(1, noteTitle);
            ResultSet foundNotes = findAllNotesInfoForNote.executeQuery();
            while (foundNotes.next()) {
                note = foundNotes.getString(COLUMN_NOTE_NOTE);
                noteDate = foundNotes.getDate(COLUMN_NOTE_DATE).toLocalDate();
                podcast = foundNotes.getString(COLUMN_PODCAST_NAME);
            }
            findAllNotesInfoForNote.close();
            return new Note(noteTitle, note, noteDate, podcast, tagsSet);
        } catch (SQLException e) {
            System.out.println("It's impossible to find notes for the " + e.getMessage());
        }
        return null;
    }

    /**
     * Finds all tags for a given title.
     *
     * @param noteTitle title for which tags will be found
     * @return Set of tags for a given title
     * @see #findAllInfoForNote(String)
     */
    private Set<String> findTagsForNote(String noteTitle) {
        String FIND_TAGS_FOR_NOTE = "SELECT " + COLUMN_TAGS_TAGS + " FROM " + TABLE_TAGS +
                " INNER JOIN " + TABLE_NOTES + " ON " + TABLE_TAGS + "." + COLUMN_TAGS_NOTE + "=" + TABLE_NOTES +
                "." + COLUMN_NOTE_ID + " WHERE " + TABLE_NOTES + "." + COLUMN_NOTE_TITLE + "=?";
        Set<String> foundTagSet = new HashSet<>();
        PreparedStatement askForNoteTitle;

        try {
            askForNoteTitle = conn.prepareStatement(FIND_TAGS_FOR_NOTE);
            askForNoteTitle.setString(1, noteTitle);
            ResultSet tagsSet = askForNoteTitle.executeQuery();
            while (tagsSet.next()) {
                String tag = tagsSet.getString(COLUMN_TAGS_TAGS);
                foundTagSet.add(tag);
            }
            askForNoteTitle.close();
            return foundTagSet;
        } catch (SQLException e) {
            System.out.println("The tags cannot be found for this note " + e.getMessage());
        }
        return null;
    }

    /**
     * Shows all podcasts present in MyNotebook.db.
     *
     * @return ObservableList of all podcasts in the MyNotebook.db
     * @see MainController#podcastsClicked()
     */
    ObservableList<String> queryAllPodcasts() {
        String QUERY_ALL_PODCASTS = "SELECT " + COLUMN_PODCAST_NAME + " FROM " + TABLE_PODCASTS;

        ObservableList<String> allPodcastList = FXCollections.observableArrayList();
        try (Statement statement = conn.createStatement();
             ResultSet podcastSet = statement.executeQuery(QUERY_ALL_PODCASTS)) {
            while (podcastSet.next()) {
                String podcastName = podcastSet.getString(COLUMN_PODCAST_NAME);
                allPodcastList.add(podcastName);
            }
            return allPodcastList;
        } catch (SQLException e) {
            System.out.println("It is impossible to print notes " + e.getMessage());
        }
        return null;
    }

    /**
     * Finds all titles in MyNotebook.db for a given podcast.
     *
     * @param podcastName Podcast for which all titles are shown
     * @return ObservableList of titles for a given podcast
     * @see MainController#elementClicked()
     * @see MainController#podcastSearchOKClicked()
     */
    ObservableList<String> findTitleForPodcast(String podcastName) {
        String FIND_NOTE_TITLES_FOR_PODCAST_NAME = "SELECT " + COLUMN_NOTE_TITLE + " FROM " + TABLE_NOTES +
                " INNTER JOIN " + TABLE_PODCASTS + " ON " + COLUMN_NOTE_PODCAST + "=" + TABLE_PODCASTS +
                "." + COLUMN_PODCAST_ID + " WHERE " + TABLE_PODCASTS + "." + COLUMN_PODCAST_NAME + "=?";
        Set<String> notesTitlesSet = new HashSet<>();
        PreparedStatement findNoteTitlesForPodcastName;

        try {
            findNoteTitlesForPodcastName = conn.prepareStatement(FIND_NOTE_TITLES_FOR_PODCAST_NAME);
            findNoteTitlesForPodcastName.setString(1, podcastName);
            ResultSet notesTitles = findNoteTitlesForPodcastName.executeQuery();
            while (notesTitles.next()) {
                String noteTitle = notesTitles.getString(COLUMN_NOTE_TITLE);
                notesTitlesSet.add(noteTitle);
            }
            findNoteTitlesForPodcastName.close();
            ObservableList<String> foundTitles = FXCollections.observableArrayList();
            foundTitles.addAll(notesTitlesSet);
            return foundTitles;
        } catch (SQLException e) {
            System.out.println("Title of notes for a given podcast was impossible " + e.getMessage());
        }
        return null;
    }

    /**
     * Finds all titles for a given tag.
     *
     * @param tagToFind Tag for which list of tiles is found
     * @return ObservableList of titles containing a tag passed as parameter
     * @see MainController#elementClicked()
     * @see MainController#tagSearchOKClicked()
     */
    ObservableList<String> findNotesForTag(String tagToFind) {
        String FIND_ALL_NOTES_INFO_FOR_TAG = "SELECT " + COLUMN_NOTE_TITLE + " FROM " +
                TABLE_TAGS + " INNER JOIN " + TABLE_NOTES + " ON " + TABLE_TAGS + "." + COLUMN_TAGS_NOTE + " = " +
                TABLE_NOTES + "." + COLUMN_NOTE_ID + " WHERE " + COLUMN_TAGS_TAGS +
                " = ?";
        Set<String> foundNotesList = new HashSet<>();
        PreparedStatement findAllNotesInfoForTag;

        try {
            findAllNotesInfoForTag = conn.prepareStatement(FIND_ALL_NOTES_INFO_FOR_TAG);
            findAllNotesInfoForTag.setString(1, tagToFind);
            ResultSet foundNotes = findAllNotesInfoForTag.executeQuery();
            while (foundNotes.next()) {
                String podcastTitle = foundNotes.getString(COLUMN_NOTE_TITLE);
                foundNotesList.add(podcastTitle);
            }
            findAllNotesInfoForTag.close();
            ObservableList<String> noteTitles = FXCollections.observableArrayList();
            noteTitles.addAll(foundNotesList);
            return noteTitles;
        } catch (SQLException e) {
            System.out.println("It's impossible to find notes for the " + e.getMessage());
        }
        return null;
    }

    /**
     * Checks if a title given as a parameter is present in MyNotebook.db.
     *
     * @param toFind Note to find if it is present in MyNotebook.db
     * @return <code>true</code> when the given title is present in MyNotebook.db.
     *          <code>false</code> otherwise
     * @see MainController#editClicked()
     * @see MainController#addNoteClicked()
     */
    boolean isTitle(Note toFind) {
        String IS_TITLE = "SELECT " + COLUMN_NOTE_TITLE + " FROM " + TABLE_NOTES + " WHERE " + COLUMN_NOTE_TITLE + "=?";
        PreparedStatement isTitleStatement;
        try {
            isTitleStatement = conn.prepareStatement(IS_TITLE);
            isTitleStatement.setString(1, toFind.getTitle());
            ResultSet titleSet = isTitleStatement.executeQuery();
            if (titleSet.isBeforeFirst()) {
                return true;
            }
            isTitleStatement.close();
        } catch (SQLException e) {
            System.out.println("The search for the given title cannot be performed");
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checks if a tag given as a parameter is present in myNotebook.db.
     *
     * @param tag a tag to be checked if is present in the database
     * @return <code>true</code> if a tag is present, <code>false</code> if it is not
     * @see MainController#tagSearchOKClicked()
     */
    boolean isTag(String tag) {
        String IS_TAG = "SELECT " + COLUMN_TAGS_TAGS + " FROM " + TABLE_TAGS + " WHERE " + COLUMN_TAGS_TAGS + "=?";
        PreparedStatement isTagStatement;
        try {
            isTagStatement = conn.prepareStatement(IS_TAG);
            isTagStatement.setString(1, tag);
            ResultSet tagSet = isTagStatement.executeQuery();
            if(tagSet.isBeforeFirst()) {
                return true;
            }
            isTagStatement.close();
        } catch (SQLException e) {
            System.out.println("The search for the the given tag cannot be performed");
            e.printStackTrace();
        }
        return false;
    }
    /**
     * Checks if a podcast given as a parameter is present in myNotebook.db.
     *
     * @param podcast a podcast to be checked if is present in the database
     * @return <code>true</code> if a podcast is present, <code>false</code> if it is not
     * @see MainController#podcastSearchOKClicked()
     */
    boolean isPodcast(String podcast) {
        String IS_PODCAST = "SELECT " + COLUMN_PODCAST_NAME + " FROM " + TABLE_PODCASTS + " WHERE " +
                COLUMN_PODCAST_NAME + "=?";
        PreparedStatement isPodcastStatement;
        try {
            isPodcastStatement = conn.prepareStatement(IS_PODCAST);
            isPodcastStatement.setString(1, podcast);
            ResultSet podcastSet = isPodcastStatement.executeQuery();
            if(podcastSet.isBeforeFirst()) {
                return true;
            }
            isPodcastStatement.close();
        } catch (SQLException e) {
            System.out.println("The search for the the given tag cannot be performed");
            e.printStackTrace();
        }
        return false;
    }
}