package khanczaryk;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;

import java.io.IOException;
import java.util.*;

/**
 * Shows <code>mainListView</code> with the list of all note's titles present in myNotebook.db. When when one of the
 * note titles is chosen, shows title, note, date, podcast and tags in <code>TextAreas</code> and
 * <code>DatePicker</code>. This data may be edited (when "Edit" is clicked) or removed (when "Remove" is clicked).
 * When the "Add new note" button is clicked - shows a new window where new data may be added.
 *<p>
 * When the button "All podcasts" clicked, shows in the <code>mainListView</code> the list of all podcasts present in
 * myNotebook.db. When one title of podcast in <code>mainListView</code> chosen, the list of all notes for the
 * chosen title of podcast is shown in <code>mainListView</code>. Finally, when one of the note titles is chosen,
 * shows title, note, date, podcast and tags in <code>TextAreas</code> and <code>DatePicker</code>.
 *<p>
 * When the button "All tags" clicked, shows in the <code>mainListView</code> the list of all tags present in
 * myNotebook.db. When a tag in <code>mainListView</code> chosen, the list of all notes for the chosen tag is shown
 * in <code>mainListView</code>. Finally, when one of the note titles is chosen, shows title, note, date, podcast and
 * tags in <code>TextAreas</code> and <code>DatePicker</code>.
 *<p>
 * When the button "All notes" clicked, shows in the <code>mainListView</code> the list of all notes present in
 * myNotebook.db. When one of the note titles is chosen, shows title, note, date, podcast and
 * tags in <code>TextAreas</code> and <code>DatePicker</code>.
 *<p>
 * When the button "Add a new note" is clicked, shows the new window.
 *<p>
 * When the button "Search" is clicked, shows 2 on the right side <code>TextAreas</code> where a user may give a tag or
 * a podcast for which a title will be searched. When OK is clicked, either the found title is shown in
 * <code>mainListView</code> or the warning alert, that the given element does not exist, is shown.
 *<p>
 * When the "Quit" is clicked - the program will be closed.
 *
 * @author kHanczaryk
 * @version %I%, %G%
 */
public class MainController {

    @FXML
    private ListView<String> mainListView;

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private HBox editRemoveHBox;

    @FXML
    private VBox mainVBox;

    @FXML
    private TextField mainNoteTitle;

    @FXML
    private TextArea mainNoteText;

    @FXML
    private DatePicker mainNoteDate;

    @FXML
    private TextField mainNotePodcast;

    @FXML
    private TextField mainNoteTags;

    @FXML
    private VBox leftVbox;

    @FXML
    private HBox bottomHBox;

    @FXML
    private VBox rightVbox;

    private Note addedOrUpdatedNote;

    @FXML
    private Label allPodcastsLabel;

    @FXML
    private Label allTagsLabel;

    @FXML
    private Label noteLabel;

    @FXML
    private VBox searchVbox;

    @FXML
    private VBox searchTagVbox;

    @FXML
    private VBox searchPodcastVbox;

    @FXML
    private TextField tagToFind;

    @FXML
    private TextField podcastToFind;

    @FXML
    private GridPane datePodcastGrid;

    private enum ListViewState {note, podcast, tag}

    private ListViewState listViewState;

    private double screenHeight;

    private double screenWidth;

    private Alert warnAlert = new Alert(Alert.AlertType.WARNING);

    private Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);

    /**
     * Sets the <code>mainListView</code>, all the buttons, labels, TextAreas and Datepicker as relative to the size of
     * the screen (the view is not distorted when the resolution of the screen is changed).
     * Shows in the <code>mainListView</code> the list of titles of all episodes kept in the MyNotebook.db.
     * Sets the addedOrUpdatedNote to an empty <code>Note</code>.
     */
    @FXML
    public void initialize() {

        addedOrUpdatedNote = new Note("", "", null, "", new HashSet<>());

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        screenHeight = primaryScreenBounds.getHeight();
        screenWidth = primaryScreenBounds.getWidth();
        mainListView.setPrefSize(screenWidth * 0.33,screenHeight * 0.581);
        allTagsLabel.setPrefHeight(screenHeight * 0.0033);
        allTagsLabel.setPadding(new Insets(screenHeight*0.0033, screenWidth*0.0033, screenHeight*0.0033,
                screenWidth*0.0033));
        leftVbox.setPadding(new Insets(screenHeight*0.0033, screenWidth*0.0033,
                screenHeight*0.0033, screenWidth*0.006));
        bottomHBox.setSpacing(screenWidth * 0.0033);
        bottomHBox.setPadding(new Insets(screenHeight * 0.01, screenWidth*0.0,
                screenHeight * 0.01, screenWidth*0.0033));
        mainNoteText.setPrefHeight(screenHeight * 0.5);
        mainVBox.setSpacing(screenHeight * 0.0132);
        mainVBox.setPadding(new Insets(0, screenWidth * 0.0132, screenHeight * 0.0132,
                screenWidth * 0.0132));
        rightVbox.setPrefHeight(screenHeight * 0.594);
        rightVbox.setPadding(new Insets(screenHeight * 0.01452, 0, 0, 0));
        editRemoveHBox.setSpacing(screenWidth * 0.00165);
        mainNotePodcast.setPrefWidth(screenWidth * 0.30);
        datePodcastGrid.setMargin(mainNoteDate, new Insets(0, screenWidth * 0.047, 0,
                screenWidth * 0.005));
        datePodcastGrid.setMargin(mainNotePodcast, new Insets(0, 0, 0,
                screenWidth * 0.005));
        showNotes();
    }

    /**
     * When in the <code>mainListView</code> the list of notes is shown and a title is chosen, shows in the
     * <code>TextAreas</code> and <code>DatePicker</code> on the right side, all data retrieved from myNotebook.db
     * for the chosen title. Makes buttons "Edit" and "Quit" visible.
     * <p>
     * When in the <code>mainListView</code> the list of podcast is shown and one element is chosen, shows in the
     * <code>mainListView</code> the list of titles for a chosen podcast.
     * <p>
     * When in the <code>mainListView</code> the list of tags is shown and one element is chosen, shows in the
     * <code>mainListView</code> the list of titles for a chosen tag.
     * <p>
     * Sets an appropriate label depending on what is presented in the <code>mainListView</code>.
     */
    @FXML
    void elementClicked() {
        String selectedElement = mainListView.getSelectionModel().getSelectedItem();
        Note selectedNote = Database.getInstance().findAllInfoForNote(selectedElement);
        allPodcastsLabel.setVisible(false);
        allTagsLabel.setVisible(false);
        noteLabel.setVisible(true);
        switch (listViewState) {
            case note:
                editRemoveHBox.setVisible(true);
                searchVbox.setVisible(false);
                showDataInTextAreas(selectedNote, mainNoteTitle, mainNoteText, mainNoteDate, mainNotePodcast,
                        mainNoteTags);
                mainVBox.setVisible(true);
                break;
            case podcast:
                Task<ObservableList<String>> podcastTask = new Task<ObservableList<String>>() {
                    @Override
                    public ObservableList<String> call() {
                        return FXCollections.observableList(Database.getInstance().
                                findTitleForPodcast(selectedElement));
                    }
                };
                mainListView.itemsProperty().bind(podcastTask.valueProperty());
                new Thread(podcastTask).start();
                setRightInvisible();
                listViewState = ListViewState.note;
                break;
            case tag:
                Task<ObservableList<String>> tagTask = new Task<ObservableList<String>>() {
                    @Override
                    public ObservableList<String> call() {
                        return FXCollections.observableList(Database.getInstance().
                                findNotesForTag(selectedElement));
                    }
                };
                mainListView.itemsProperty().bind(tagTask.valueProperty());
                new Thread(tagTask).start();
                setRightInvisible();
                listViewState = ListViewState.note;
                break;
        }
    }

    /**
     * Shows a <code>DialogPane</code> (<code>addOrEditNote.fxml</code> and <code>AddOrEditNoteController</code>) with OK
     * and Cancel buttons. Format the content of the <code>DialogPane</code>, so that its children are proportional
     * to the screen resolution. Removes from MyNotebook.db the old version of the note selected for editing, checks
     * if all fields are filled (if not, shows the "No input warning" alert and adds to MyNotebook.db the initial
     * version of the note), checks if updated note has a unique name (if not, shows the "The same title conflict"
     * warning alert and adds to MyNotebook.db the initial version of the note), adds to MyNotebook.db the updated note,
     * shows an information alert that the update was made. Shows the updated list of titles in the
     * <code>mainListView</code>.
     *
     * @see AddOrEditNoteController
     */

    @FXML
    void editClicked() {

        String noteToChange = mainListView.getSelectionModel().getSelectedItem();
        Note oldNote = Database.getInstance().findAllInfoForNote(noteToChange);
        Database.getInstance().removeNote(noteToChange);
        showFormAndHandleData(oldNote, "Edit confirmation", "The note was updated");

    }

    /**
     * Removes from MyNotebook.db the note with the title selected in the <code>mainListView</code>, shows
     * <code>dialogPane</code> (<code>removeNote.fxml</code> and <code>removeNoteController</code>) to confirm or
     * to cancel the removal. Updates the <code>mainListView</code>. Deselects the title from the
     * <code>mainListView</code>, makes buttons Edit and Remove invisible, clears all <code>TextField</code>,
     * <code>TextArea</code> and <code>DatePicker</code>.
     *
     * @see RemoveNoteController
     */
    @FXML
    void removeClicked() {
        String noteToRemove = mainListView.getSelectionModel().getSelectedItem();
        Dialog<ButtonType> dialog = createDialogPane();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("removeNote.fxml"));
            dialog.getDialogPane().setContent(root);
        } catch (IOException e) {
            System.out.println("Couldn't load the removeNote dialog pane");
            e.printStackTrace();
        }
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Task<ObservableList<String>> removeTask = new Task<ObservableList<String>>() {
                @Override
                public ObservableList<String> call() {
                    Database.getInstance().removeNote(noteToRemove);
                    return FXCollections.observableList(Database.getInstance().findAllNotes());
                }
            };
            mainListView.itemsProperty().bind(removeTask.valueProperty());
            new Thread(removeTask).start();
        }
        clearAllFields();
    }

    /**
     * Shows a <code>DialogPane</code> (<code>addOrEditNote.fxml</code> and <code>AddOrEditNoteController</code>) with OK
     * and Cancel buttons. Format the content of the <code>DialogPane</code>, so that its children are proportional
     * to the screen resolution. Populate the <code>TextAreas</code> with the information retrieved from the
     * <code>Note</code> (first - with the empty Strings). When the button OK is clicked, checks if all the fields
     * are filled (if no - shows a warning alert and calls itself), checks if the title is unique (if not - shows
     * a warning alert and calls itself). If the conditions are met, adds the new note to the database, clears all
     * the <code>TextAreas</code> and <code>DatePicker</code>. Deselects the title from the <code>mainListView</code>,
     * makes buttons Edit and Remove invisible, clears all <code>TextArea</code> and <code>DatePicker</code> in the
     * <code>mainBorderPane</code>.
     *
     * @see AddOrEditNoteController
     */
    @FXML
    void addNoteClicked() {
        showFormAndHandleData(addedOrUpdatedNote, "Adding confirmation",
                "New note added to the database");
    }

    /**
     * Upon the click on the button "Podcasts" in <code>mainListView</code> shows the list of all podcasts present in
     * myNotebook.db. Sets a label above the <code>mainListView</code> to "All podcasts".
     */
    @FXML
    void podcastsClicked() {
        Task<ObservableList<String>> podcastTask = new Task<ObservableList<String>>() {
            @Override
            public ObservableList<String> call() {
                return FXCollections.observableList(Database.getInstance().queryAllPodcasts()); }
            };
        new Thread(podcastTask).start();
        mainListView.itemsProperty().unbind();
        mainListView.itemsProperty().bind(podcastTask.valueProperty());
        allPodcastsLabel.setVisible(true);
        allTagsLabel.setVisible(false);
        noteLabel.setVisible(false);
        listViewState = ListViewState.podcast;
        setRightInvisible();
    }

    /**
     * Upon the click on the button "Tags" in <code>mainListView</code> shows the list of all tags present in
     * myNotebook.db. Sets a label above the <code>mainListView</code> to "All tags".
     */
    @FXML
    void tagsClicked() {
        Task<ObservableList<String>> tagTask = new Task<ObservableList<String>>() {
            @Override
            public ObservableList<String> call() {
                return FXCollections.observableList(Database.getInstance().findAllTags()); }
        };
        mainListView.itemsProperty().unbind();
        mainListView.itemsProperty().bind(tagTask.valueProperty());
        new Thread(tagTask).start();
        allPodcastsLabel.setVisible(false);
        allTagsLabel.setVisible(true);
        noteLabel.setVisible(false);
        listViewState = ListViewState.tag;
        setRightInvisible();
    }

    /**
     * Upon the click on the button "Search", makes <code>rightVbox</code> clean (if there is anything) and shows the
     * formatted <code>searchVbox</code>: <code>Label</code> "Give a tag to find", <code>Label</code>
     * "Give a podcast to find", <code>TextAreas</code> where the searched elements should be given by a user and
     * <code>Buttons</code> "OK" to accept the input given by a user.
     */
    @FXML
    void searchClicked() {
        mainVBox.setVisible(false);
        searchVbox.setVisible(true);
        podcastToFind.setPrefHeight(screenHeight*0.033);
        tagToFind.setPrefHeight(screenHeight*0.033);
        searchTagVbox.setSpacing(screenHeight * 0.0132);
        searchVbox.setPadding(new Insets(screenWidth*0.0132));
        searchPodcastVbox.setSpacing(screenHeight*0.0132);
        searchTagVbox.setPadding(new Insets(0.0, 0.0, 0.0495*screenHeight, 0.0));
        podcastToFind.setPromptText("Type a podcast to find...");
        tagToFind.setPromptText("Type a tag to find...");
        tagToFind.clear();
        podcastToFind.clear();
        editRemoveHBox.setVisible(false);
    }

    /**
     * Upon the click on the <code>Button</code> "OK" below the <code>TextArea</code> where a user may give a tag to
     * find, shows in the <code>mainListView</code> the list of titles for a given tag. If the given tag does not
     * exist in the myNotebook.db shows a warning alert. Updates the label above the <code>mainListView</code> and
     * clears the <code>TextAreas</code> where a user can give a searched tag or podcast.
     */
    @FXML
    void tagSearchOKClicked() {
        podcastToFind.clear();
        String tag = tagToFind.getText();
        if (Database.getInstance().isTag(tag)) {
            Task<ObservableList<String>> newTask = new Task<ObservableList<String>>() {
                @Override
                public ObservableList<String> call() {
                    return FXCollections.observableList(Database.getInstance().findNotesForTag(tag));
                }
            };
            mainListView.itemsProperty().unbind();
            mainListView.itemsProperty().bind(newTask.valueProperty());
            new Thread(newTask).start();
            listViewState = ListViewState.note;
        } else {
            showAlert(warnAlert, "No such a tag", "The given tag is not present in the database.");
            tagToFind.clear();
        }
    }

    /**
     * Upon the click on the <code>Button</code> "OK" below the <code>TextArea</code> where a user may give a podcast to
     * find, shows in the <code>mainListView</code> the list of titles for a given podcast. If the given podcast
     * does not exist in the myNotebook.db shows a warning alert. Updates the label above the <code>mainListView</code>
     * and clears the <code>TextAreas</code> where a user can give a searched tag or podcast.
     */
    @FXML
    void podcastSearchOKClicked() {
        tagToFind.clear();
        String podcast = podcastToFind.getText();
        if (Database.getInstance().isPodcast(podcast)) {
            Task<ObservableList<String>> newTask = new Task<ObservableList<String>>() {
                @Override
                public ObservableList<String> call() {
                    return FXCollections.observableList(Database.getInstance().findTitleForPodcast(podcast));
                }
            };
            mainListView.itemsProperty().unbind();
            mainListView.itemsProperty().bind(newTask.valueProperty());
            new Thread(newTask).start();
            listViewState = ListViewState.note;
        } else {
            showAlert(warnAlert, "No such a podcast", "The given podcast is not present in the database.");
            podcastToFind.clear();
        }
    }

    /**
     * Quits the program.
     */
    @FXML
    void quit() {
        try {
            Platform.exit();
        } catch (Exception e) {
            System.out.println("Problems in quitting");
            e.printStackTrace();
        }
    }

    /**
     * Deselects the title from the <code>mainListView</code>, makes buttons "Edit" and "Remove" invisible, clears all
     * <code>TextAreas</code> and <code>DatePicker</code>.
     *
     * @see editClicked()
     * @see removeClicked()
     * @see addNoteClicked()
     */
    private void clearAllFields() {
        mainListView.getSelectionModel().clearSelection();
        editRemoveHBox.setVisible(false);
        mainNoteTitle.clear();
        mainNoteText.clear();
        mainNoteDate.setValue(null);
        mainNotePodcast.clear();
        mainNoteTags.clear();
        searchVbox.setVisible(false);
    }

    /**
     * Retrieves set of tags from the given note, generates a list of tags divided by commas and shows them in the
     * given <code>TextArea</code>.
     *
     * @param Note note for which tags need to be retrieved
     * @param TextArea TextArea where the retrieved tags will be shown
     * @see showDataInTextAreas()
     */
    private void showTagsInTextArea(Note selectedNote, TextField whereToShow) {
        int i = 0;
        StringBuilder tagsToShow = new StringBuilder();
        List<String> tagsList = new ArrayList<>();
        tagsList.addAll(selectedNote.getTags());
        if (!tagsList.isEmpty()) {
            while (i < tagsList.size() - 1) {
                tagsToShow.append(tagsList.get(i));
                tagsToShow.append(", ");
                i++;
            }
            tagsToShow.append(tagsList.get(i));
            whereToShow.setText(tagsToShow.toString());
        }
    }

    /**
     * Retrieves data from the gievn <code>Note</code> and shows it in the <code>TextArea</code> and
     * <code>DatePicker</code> accordingly.
     *
     * @param selectedNote Note for which data is retrieved
     * @param title TextArea where title retrieved from Note is to be shown
     * @param text TextArea where text retrieved from Note is to be shown
     * @param date DatePicker where date retrieved from Note is to be shown
     * @param podcast TextArea where podcast retrieved from Note is to be shown
     * @param tags TextArea where tags retrieved from Note are to be shown
     * @see elementClicked()
     *
     */
    protected void showDataInTextAreas(Note selectedNote, TextField title, TextArea text, DatePicker date,
                                     TextField podcast, TextField tags) {
        title.setText(selectedNote.getTitle());
        text.setText(selectedNote.getNote());
        date.setValue(selectedNote.getDate());
        podcast.setText(selectedNote.getPodcast());
        showTagsInTextArea(selectedNote, tags);
    }

    /**
     * Generates a new <code>Note</code> from <code>TextAreas</code> and <code>DatePicker</code> which are shown in
     * any <code>BorderPane</code>.
     *
     * @param title TextArea from which title for the Note is retrieved
     * @param text TextArea from which text for the Note is retrieved
     * @param date DatePicker from which date for the Note is retrieved
     * @param podcast TextArea from which podcast for the Note is retrieved
     * @param tags TextArea from which tags for the Note are retrieved
     * @return Note generated from the data given by the user in the TextAreas and DatePicker in a BorderPane
     *
     * @see editClicked()
     * @see AndNewNoteController#getNoteContent()
     */
    Note createNoteFromUserInput(TextField title, TextArea text, DatePicker date, TextField podcast,
                                        TextField tags) {
        String[] splitTags = tags.getText().split(",");
        for (int i = 0; i < splitTags.length; i++) {
            splitTags[i] = splitTags[i].trim();
            splitTags[i] = splitTags[i].toLowerCase();
        }
        return new Note(title.getText(), text.getText(), date.getValue(), podcast.getText(),
                new HashSet<>(Arrays.asList(splitTags)));
    }

    /**
     * Generates resizable <code>Dialog<ButtonType></code> with OK and CANCEL buttons, blocks the parental window.
     *
     * @return resizable Dialog<ButtonType> with OK and CANCEL buttons
     *
     * @see addNoteClicked()
     * @see removeClicked()
     *
     */
    private Dialog<ButtonType> createDialogPane() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setX(screenHeight*0.594);
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setResizable(false);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        return dialog;
    }

    /**
     * Shows in the <code>mainListView</code> the list of all notes. Updates the label and makes the right part of
     * the window invisible.
     *
     * @see initialize()
     */
    public void showNotes() {
        Task<ObservableList<String>> myTask = new Task<ObservableList<String>>() {
            @Override
            public ObservableList<String> call() {
                return FXCollections.observableList(Database.getInstance().findAllNotes());
            }
        };
        new Thread(myTask).start();
        mainListView.itemsProperty().bind(myTask.valueProperty());
        allPodcastsLabel.setVisible(false);
        allTagsLabel.setVisible(false);
        noteLabel.setVisible(true);
        listViewState = ListViewState.note;
        setRightInvisible();
    }

    /**
     * Makes the right part of the window invisible.
     *
     * @see showNotes()
     * @see tagsClicked()
     * @see podcastsClicked()
     * @see elementClicked()
     */
    private void setRightInvisible() {
        searchVbox.setVisible(false);
        editRemoveHBox.setVisible(false);
        mainVBox.setVisible(false);

    }

    /**
     * Creates alerts.
     *
     * @see editClicked()
     * @see addNoteClicked()
     * @see tagSearchOKClicked()
     * @see podcastSearchOKClicked()
     */

    private void showAlert(Alert alert, String title, String text) {
         alert.setTitle(title);
         alert.setContentText(text);
         alert.showAndWait();
     }

    /**
     * Shows a <code>DialogPane</code> (<code>addOrEditNote.fxml</code> and <code>AddOrEditNoteController</code>) with OK
     * and Cancel buttons. Format the content of the <code>DialogPane</code>, so that its children are proportional
     * to the screen resolution. Populate the <code>TextAreas</code> with the information retrieved from the
     * <code>Note</code> (first - with the empty Strings). When the button OK is clicked, checks if all the fields
     * are filled (if no - shows a warning alert and calls itself), checks if the title is unique (if not - shows
     * a warning alert and calls itself). If the conditions are met, adds the new note to the database, clears all
     * the <code>TextAreas</code> and <code>DatePicker</code>. Deselects the title from the <code>mainListView</code>,
     * makes buttons Edit and Remove invisible, clears all <code>TextArea</code> and <code>DatePicker</code> in the
     * <code>mainBorderPane</code>.
     *
     * @see AddOrEditNoteController
     */

    private void showFormAndHandleData(Note initialNoteToDisplay, String confirmationTitle, String confirmationText) {
        Dialog<ButtonType> dialog = createDialogPane();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("addOrEditNote.fxml"));
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
        }
        AddOrEditNoteController controller = fxmlLoader.getController();
        controller.setMyMainController(this);
        controller.getBorderPaneAdd().setPrefSize(screenWidth * 0.33, screenHeight * 0.581);
        controller.getNewNote().setPrefHeight(screenHeight * 0.33);
        controller.getNewTags().setPrefHeight(screenHeight * 0.033);
        controller.getNewTitle().setPrefHeight(screenHeight * 0.033);
        controller.getNewPodcast().setPrefHeight(screenHeight * 0.033);
        controller.getvBoxAdd().setSpacing(screenHeight * 0.01);
        controller.getvBoxAdd().setPadding(new Insets(screenHeight * 0.0033));

        showDataInTextAreas(initialNoteToDisplay, controller.getNewTitle(), controller.getNewNote(),
                controller.getNewDate(), controller.getNewPodcast(), controller.getNewTags());

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            addedOrUpdatedNote = controller.getNoteContent();
            if (addedOrUpdatedNote.isIncomplete()) {
                showAlert(warnAlert, "No input warning", "All the fields need to be filled!");
                addNoteClicked();
            } else if (!Database.getInstance().isTitle(addedOrUpdatedNote)) {

                Task<ObservableList<String>> addTask = new Task<ObservableList<String>>() {
                    @Override
                    public ObservableList<String> call() {
                        Database.getInstance().addNewNote(addedOrUpdatedNote);
                        return Database.getInstance().findAllNotes();
                    }
                };
                mainListView.itemsProperty().bind(addTask.valueProperty());
                new Thread(addTask).start();
                showAlert(infoAlert, confirmationTitle, confirmationText);
                addedOrUpdatedNote.setDate(null);
                addedOrUpdatedNote.setNote("");
                addedOrUpdatedNote.setPodcast("");
                addedOrUpdatedNote.setTitle("");
                addedOrUpdatedNote.setTags(new HashSet<>());
            } else {
                showAlert(warnAlert, "The replicate of title", "The title needs to be unique. " +
                        "Give another title!");
                addNoteClicked();
            }
            clearAllFields();
        }
    }
}