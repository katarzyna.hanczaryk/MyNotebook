package khanczaryk;

/**
 * Shows a window to confirm a removal of a note. When OK clicked: the note is to be removed. When cancel: the action
 * is cancelled.
 *
 * @author kHanczaryk
 * @version %I%, %G%
 */
public class RemoveNoteController {

}
