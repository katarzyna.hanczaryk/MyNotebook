package khanczaryk;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void init() throws Exception {
        if (!Database.getInstance().openConnection()) {
            System.out.println("FATAL ERROR: couldn't connect to database.");
            Platform.exit();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        setUserAgentStylesheet(STYLESHEET_CASPIAN);
        primaryStage.setTitle("MyNotebook");
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setWidth(primaryScreenBounds.getWidth()*0.90);
        primaryStage.setHeight(primaryScreenBounds.getHeight()*0.54);
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setScene(new Scene(root, primaryScreenBounds.getWidth(),
                primaryScreenBounds.getHeight()));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        Database.getInstance().closeConnection();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
